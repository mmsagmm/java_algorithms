public class FibonacciSeq {
    public static int fibonacci(int i) {
        if (i <= 1) return i;
        return fibonacci(i - 1) + fibonacci(i - 2);
    }

    public static void main(String[] args) {
        System.out.println(1 + "-" + fibonacci(1));
        System.out.println(2 + "-" + fibonacci(2));
        System.out.println(3 + "-" + fibonacci(3));
        System.out.println(4 + "-" + fibonacci(4));
        System.out.println(5 + "-" + fibonacci(5));
        System.out.println(6 + "-" + fibonacci(6));
        System.out.println(7 + "-" + fibonacci(7));
    }
}
