public class atoi {
    final String int_max = "2147483647";

    public int myAtoi(String input) {

        if (input.length() == 0) return 0;

        boolean positive = true;
        boolean digitsStarted = false;
        int seqEnd = 0;
        int seqStart = 0;

        for (int i = 0; i < input.length(); i++) {
            char symbol = input.charAt(i);

            if (!isSymbolSpace(symbol)
                    && !isSymbolDigit(symbol)
                    && !isSymbolSide(symbol)
                    && !digitsStarted) {
                break;
            }

            if (!isSymbolDigit(symbol) && digitsStarted) {
                break;
            }

            if (isSymbolDigit(symbol) && digitsStarted) {
                seqEnd++;
            }

            if (isSymbolDigit(symbol) && !digitsStarted) {
                seqStart = i;
                seqEnd = i + 1;
                digitsStarted = true;
            }

            if (isSymbolSide(symbol) && !digitsStarted) {
                if (i + 1 >= input.length() || !isSymbolDigit(input.charAt(i + 1))) {
                    return 0;
                }
                positive = isPositive(symbol);
                digitsStarted = true;
                seqStart = i + 1;
                seqEnd = i + 1;
            }
        }

        if (seqEnd - seqStart < 1) return 0;
        var value = 0;
        try {
            var str = input.substring(seqStart, seqEnd);
            var trailing = 0;
            for (int i = 0; i < str.length(); i++) {
                if (str.charAt(i) != '0') break;
                trailing++;
            }

            if (str.length()-trailing <= 0) return 0;

            str = str.substring(trailing, str.length()-1);

            if (str.length() > int_max.length()) {
                value = positive ? Integer.MAX_VALUE : Integer.MIN_VALUE;
            } else {
                value = Integer.parseInt(str);
            }
        } catch (NumberFormatException ex) {
            value = positive ? Integer.MAX_VALUE : Integer.MIN_VALUE;
        }
        if (positive && value >= Integer.MAX_VALUE) {
            return Integer.MAX_VALUE;
        }

        if (!positive && value * -1 <= Integer.MIN_VALUE) {
            return Integer.MIN_VALUE;
        }

        var side = positive ? 1l : -1l;
        return (int) (digitsStarted ? value * side : 0);
    }

    private boolean isSymbolDigit(char symbol) {
        return symbol <= '9' && symbol >= '0';
    }

    private boolean isSymbolSide(char symbol) {
        return symbol == '-' || symbol == '+';
    }

    private boolean isSymbolSpace(char symbol) {
        return symbol == ' ';
    }

    private boolean isPositive(char symbol) {
        if (symbol == '+') {
            return true;
        } else if (symbol == '-') {
            return false;
        } else {
            throw new IllegalArgumentException("Symbol " + symbol + " unparsable");
        }
    }

    public static void main(String[] args) {
        atoi func = new atoi();
        var result = func.myAtoi("-91283472332");
        System.out.println(result);
    }
}
