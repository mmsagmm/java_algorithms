public class SwapPairsInLinkedList {
    public static ListNode swapPairs(ListNode head) {
        if (head == null || head.next == null) return head;

        ListNode prev = null;
        ListNode prevPrev = null;

        ListNode newHead = null;

        ListNode cur = head;
        while (cur != null) {

            if (prev == null) {
                prev = cur;
                cur = cur.next;
            } else {
                newHead = newHead == null ? cur : newHead;

                prev.next = cur.next;
                cur.next = prev;
                if (prevPrev != null) {
                    prevPrev.next = cur;
                }

                prevPrev = prev;

                cur = prev.next;

                prev = null;
            }

        }

        return newHead != null ? newHead : head;
    }

    public static void main(String[] args) {
        int[] input = new int[]{1, 2, 3, 4, 5, 6};
        ListNode result = swapPairs(BuildList(input));

        printResult(result);
    }

    public static void printResult(ListNode input) {
        ListNode cur = input;
        String output = "";
        while (cur != null) {
            output = output + cur.val + ", ";
            cur = cur.next;
        }

        System.out.println(output);
    }

    public static ListNode BuildList(int[] arr) {
        ListNode head = new ListNode(arr[0]);
        ListNode cur = head;

        for (int i = 1; i < arr.length; i++) {
            cur.next = new ListNode(arr[i]);
            cur = cur.next;
        }

        return head;
    }
}

