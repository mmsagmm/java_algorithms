public class MinimumValueToGetPositiveStepByStepSum {
    public int invoke(int[] input) {
        int[] runningSum = new int[input.length];
        runningSum[0] = input[0];
        int minRunningSum = input[0];

        for (int i = 1; i < input.length; i++) {
            runningSum[i] = runningSum[i - 1] + input[i];
            if (runningSum[i] < minRunningSum) {
                minRunningSum = runningSum[i];
            }
        }

        return minRunningSum > 0 ? 1 : Math.abs(minRunningSum)+1;
    }
}
