public class RunningSumOf2DArray {
    public int[] invoke(int[] input) {

        int[] target = new int[input.length];
        target[0] = input[0];
        for (int i = 1; i < input.length; i++) {
            target[i] = target[i - 1] + input[i];
        }

        return target;
    }
}
