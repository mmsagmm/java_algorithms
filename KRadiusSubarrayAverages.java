public class KRadiusSubarrayAverages {
    public int[] getAverages(int[] nums, int k) {

        var runningSum = new long[nums.length];
        runningSum[0] = nums[0];

        for (int i = 1; i < nums.length; i++) {
            runningSum[i] = runningSum[i - 1] + nums[i];
        }

        var target = new int[nums.length];
        for (int i = 0; i < nums.length; i++) {
            if(k == 0){
                target[i] = nums[i];
                continue;
            }
            //check condition
            if (i < k || nums.length - i <= k) {
                target[i] = -1;
            } else {
                double value = ((runningSum[i+k] - runningSum[i-k]+ nums[i-k])/(double)(k*2+1));
                target[i] = (int)value;
            }

        }

        return target;
    }
}
