import java.util.ArrayList;
import java.util.HashSet;

public class GreatestCommonDivider {
    private static int invoke(int i, int j) {
        ArrayList<Integer> iDividers = new ArrayList<Integer>();
        ArrayList<Integer> jDividersArr = new ArrayList<Integer>();

        lookForDividers(i, i, iDividers);
        lookForDividers(j, j, jDividersArr);

        HashSet<Integer> jDividers = new HashSet<>(jDividersArr);

        for (int k = 0; k <= iDividers.size(); k++) {
            Integer item = iDividers.get(k);
            if (jDividers.contains(item)) {
                return item;
            }
        }

        return -1;
    }

    private static void lookForDividers(int originalValue, int i, ArrayList<Integer> accumulator) {
        if (i <= 0) return;

        int divideLeftover = originalValue % i;

        if (divideLeftover == 0) accumulator.add(i);

        lookForDividers(originalValue, i - 1, accumulator);
    }

    public static void main(String[] args) {
        System.out.println(invoke(36, 54));
    }
}
